#include "SRRGraphClient.h"

SRRGraphClient::SRRGraphClient(WiFiClient& client, const char* host, uint8_t port)
{
  _client = &client;
  _host = host;
  _port = port;
}

void  SRRGraphClient::logDataToServer(String date, float temperatureLow,  float temperatureHigh)
{
  if (!_client->connect(_host, _port)) {
    Serial.println("Connection failed");
    return;
  }

  String url = "/csv/temperatures.csv";
  String cmd = "{\"Date\": \"" + date + "\", \"High\":\"" + String(temperatureLow) + "\", \"" + String(temperatureHigh) + "\":\"10\"}";

  int contLen = cmd.length();
  _client->print("POST " + url + " HTTP/1.1\r\n" +
                 "Host: " + _host + "\r\n" +
                 "Connection: keep-alive\r\n" +
                 "Accept: */*\r\n" +
                 "Content-Type: application/json\r\n" +
                 "Content-Length: " + contLen + "\r\n\r\n" + cmd + "\r\n\r\n");
  delay(100);
}


