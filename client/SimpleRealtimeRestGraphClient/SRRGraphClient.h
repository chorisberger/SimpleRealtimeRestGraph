#include <WiFiClient.h>

class SRRGraphClient
{
  public:
    SRRGraphClient(WiFiClient& client, const char* host, uint8_t port);
    
    void logDataToServer(String date, float temperatureLow,  float temperatureHigh);

  private:
  	const char* _host;
    uint8_t _port;
    WiFiClient* _client;
};



