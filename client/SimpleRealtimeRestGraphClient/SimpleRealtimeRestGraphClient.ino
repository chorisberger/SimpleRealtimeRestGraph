#include <WiFi.h>
#include <WiFiClient.h>
#include <Wire.h>
#include "SRRGraphClient.h"

// WIRELESS SETTIGNS
const char* ssid     = "<your wifi ssid>";
const char* password = "<your wifi pwd>";
const char* server = "<your remove server ip>";

// PRIVATE MEMBERS
WiFiClient client;
SRRGraphClient reporter = SRRGraphClient(client, server, 8000);

void setup() {
  Serial.begin(9600);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
}

void loop() {
  reporter.logDataToServer("20180102", 100, 200);
  delay(2000);
}
