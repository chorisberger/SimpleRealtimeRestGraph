#!/usr/bin/env python
# -*- coding: utf-8 -*-

import SimpleHTTPServer
import SocketServer
import urlparse
import csv
import json
import os.path

#
# Simple Realtime Rest Graph Server
# 
# A simple python server that displays a graph that can be updated using POST requests
# The http://dygraphs.com javascript library is used to display the data.
# 2018 by C. Horisberger
#
# Usage: 
#	python simpleRealtimeRestGraphServer.py
# Add datapoint to csv: 
#	curl -X POST -H "Content-Type: application/json" -d '{"Date":"20180101", "High":"62", "Low":"10"}' "http://localhost:8000/csv/temperatures.csv"
#   Also check out the client folder to see an example using an arduino compatible device
# Open graph: 
# 	open http://localhost:8000/graph.html in browser
#
# This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults." 
# THE PROVIDER makes no representations or warranties of any kind concerning the safety, suitability, lack of viruses, inaccuracies, typographical errors, 
# or other harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use of any software, and you are solely responsible for determining whether this SOFTWARE PRODUCT 
# is compatible with your equipment and other software installed on your equipment. You are also solely responsible for the protection of your equipment and backup of your data, and 
# THE PROVIDER will not be liable for any damages you may suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT.
#

PORT = 8000

class GetHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):

  	def do_POST(self):
  		try:
			parsedParams = urlparse.urlparse(self.path)
			queryParsed = urlparse.parse_qs(parsedParams.query)
			path = parsedParams.path[1:]
	
			self.data_string = self.rfile.read(int(self.headers['Content-Length']))
			data = json.loads(self.data_string)
			print "recieved new datapoint: " + str(data )
	
			keys = list()
			values = list()
			for key, value in sorted(data.iteritems()):
				keys.append(key)
				values.append(value)
			header = ",".join(keys)
	
			if not os.path.isfile(path):
				myfile = open(path, 'a')
				myfile.write(header + "\n")
				print "Created new file in " + path
			else:
				myfile = open(path, 'a')
	
			wr = csv.writer(myfile, quoting=csv.QUOTE_NONE)
			wr.writerow(values)
			myfile.flush()
		
			self.send_response(200)
			self.send_header('Content-type', 'text/html')
			self.end_headers()
			self.wfile.write('{"status" : "ok"}')
		except:
			self.send_response(500)
			self.send_header('Content-type', 'text/html')
			self.end_headers()
			self.wfile.write('{"status" : "failed"}')
			
		return

	def do_GET(self):
		SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)

Handler = GetHandler
httpd = SocketServer.TCPServer(("", PORT), Handler)
httpd.allow_reuse_address = True

running = True
try:
	while running:
		httpd.handle_request()
except (KeyboardInterrupt, SystemExit):
	print "shutting down..."
	httpd.shutdown
	running = False
	
